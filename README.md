<div align="center">
<img src="https://i.imgur.com/5cr9ceC.png" alt="Object Explorer" width="500"/><br><br>
A custom [Dendron](https://wiki.dendron.so) pod for exporting "cleanly" to [Hugo](https://github.com/fenneclab/hugo-bin).
<br><br>
⚠ Subject to breaking changes because it's a hodgepodge mess. Use at your own risk. ⚠
</div>


## Features
- This pod is the same as the built-in HTML pod, except:
    - It preserves frontmatter!!!<sup>mostly</sup>
    - It processes the file structure and all links into what Hugo likes
    - Backlinks, children, and original filename are included as arrays in the frontmatter (so you have greater control via shortcodes)
- A new `enableFMTitle` frontmatter tag enables/disables title rendering (which is nice for custom titles)

## Installation
1. Set up NodeJS in your Dendron workspace.
2. Set up Hugo in your Dendron workspace. With a theme. [hugo-book](https://github.com/alex-shpak/hugo-book) is a good choice.
3. `npm i @dendronhq/dendron-cli`
4. `npm i dendron-hugo-pod`
5. `npx dendron exportPod --podId hugo --podPkg dendron-hugo-pod --podSource custom --config dest=content,assets=static`
6. `hugo server` or `hugo`

Some hugo config:
```yaml
markup:
  goldmark:
    renderer:
      unsafe: true # render html in md files
  markup:
  tableOfContents: # render more than h1-h3 in toc
    startLevel: 2
    endLevel: 6
staticDir: notes # eliminate need to copy the assets folder - depends on your needs, though

taxonomies: # set hugo categories/tags page names to something you will never use so the dendron tag page will actually work
  category: ebrbbrbrbrbrbrbrrbrbr
  tag: ebrbbrbrbrbrbrbrrbrbr
disableKinds:
  - "taxonomy"
  - "term"
```

Some additional hugo config for `hugo-books`, specifically
```yaml
params:
  BookTheme: auto # auto light/dark theme
  BookPortableLinks: true # don't break when .md is used in links
  BookSection: "*" # render all files in site map
```

## Configuration Options
- `dest` - content directory
- `assets` - static assets directory
    - defaults to not copying assets at all
- `asset_prefix` - path section to prepend to images on sites where the site isn't actually at the root
    - defaults to using the asset prefix in dendron.yml
- `nav_exclude` - equivalent hugo frontmatter tag to dendron's `nav_exclude` tag
- `nav_order` - equivalent hugo frontmatter tag to dendron's `nav_order` tag
- `nav_collapse` - hugo frontmatter tag for allowing files to collapse in the sidebar (looking at you, `hugo-books`)

## Caveats
- Headings that aren't in the format "`### name`" are not guaranteed to work with tables of contents.
- Don't put empty lines in mermaid blocks or Hugo doesn't render them.
- Schemas aren't supported.
- Note references/embeds need custom styling to look correct but are supported properly.
- Your asset prefix should be prefixed with a `/` so it looks globally instead of relative to the current page.

## Project Status
Hopefully, this project is complete. It may change in the future if I come across something that needs adding or fixing.

## License
This project is licensed under the GPLv3 license.

Large amounts of the code are based on Dendron's built-in pods.
