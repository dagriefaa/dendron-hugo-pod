import {
    ExportPodConfig,
    PublishPodConfig,
} from "@dendronhq/pods-core";

export type HugoPublishPodConfig = PublishPodConfig & {
    names: { [key: string]: string }
    paths: { [key: string]: string }
    ids: { [key: string]: string }
    nav_exclude?: string
    nav_collapse?: string
    nav_order?: string
    asset_prefix: string
}

export type HugoExportPodConfig = ExportPodConfig & HugoPublishPodConfig & {
    assets?: string,
}

export type Url = {
    href: string
}
