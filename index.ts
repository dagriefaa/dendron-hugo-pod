import {
    FOLDERS,
    VaultUtils
} from "@dendronhq/common-all";
import {
    ExportPod,
    ExportPodPlantOpts,
    PodUtils
} from "@dendronhq/pods-core";
import fs from "fs-extra";
import path from "path";
import { JSONSchemaType } from "ajv";
import { HugoPublishPod } from "./HugoPublishPod";
import { HugoExportPodConfig } from "./Types";

export const ID = "hugo";

export class HugoExportPod extends ExportPod<HugoExportPodConfig> {
    static id: string = ID;
    static description: string = "export notes to hugo";

    get config(): JSONSchemaType<HugoExportPodConfig> {
        return PodUtils.createExportConfig({
            required: [],
            properties: {
                assets: {
                    type: "string",
                    description: "path to put asset folder in",
                    default: "static",
                    nullable: true
                },
                nav_exclude: {
                    type: "string",
                    description: "hugo frontmatter key for nav_exclude",
                    default: "bookHidden",
                    nullable: true
                },
                nav_collapse: {
                    type: "string",
                    description: "hugo frontmatter key for collapsible sitemap entries",
                    default: "bookCollapseSection",
                    nullable: true
                },
                nav_order: {
                    type: "number",
                    description: "hugo frontmatter key for nav_order",
                    default: "weight",
                    nullable: true
                },
                asset_prefix: {
                    type: "string",
                    description: "asset prefix path for sites that aren't at the actual site root",
                    default: "",
                    nullable: true
                }
            },
        }) as JSONSchemaType<HugoExportPodConfig>;
    }

    async plant(opts: ExportPodPlantOpts<HugoExportPodConfig>) {
        const { dest, notes, vaults, wsRoot, config } = opts;
        const { assets = "static" } = config;

        let start = performance.now()

        // verify dest exist
        fs.ensureDirSync(path.dirname(dest.fsPath));
        const mdPublishPod = new HugoPublishPod();

        let folders = notes.reduce(
            (acc, note) => acc.add(note.fname.split('.').slice(0, -1).join('.')),
            new Set()
        );
        opts.config.paths = notes.reduce((acc, note) => ({
            ...acc,
            [note.fname]: (note.fname === "root")
                ? "_index.md"
                : note.fname.replace(/\./g, "/") + (folders.has(note.fname) ? "/_index.md" : ".md")
        }), {})
        opts.config.names = notes.reduce((acc: { [key: string]: string }, note) => ({ ...acc, [note.fname]: note.title }), {})
        opts.config.ids = notes.reduce((acc, note) => ({
            ...acc,
            [note.id]: `/${note.fname.replace(/\./g, "/")}`
        }), {});

        await Promise.all(
            notes.map(async (note) => {

                const body = await mdPublishPod.plant({ ...opts, note });

                const fpath = path.join(dest.fsPath, opts.config.paths[note.fname]);
                await fs.ensureDir(path.dirname(fpath));
                return fs.writeFile(fpath, body);
            })
        );

        // Export Assets
        if (assets !== "") {
            await Promise.all(
                vaults.map(async (vault) => {
                    const destPath = path.join(
                        assets,
                        FOLDERS.ASSETS
                    );
                    const srcPath = path.join(
                        wsRoot,
                        VaultUtils.getRelPath(vault),
                        FOLDERS.ASSETS
                    );
                    if (fs.pathExistsSync(srcPath)) {
                        await fs.copy(srcPath, destPath);
                    }
                })
            );
        }

        let end = performance.now()
        console.log(`Export took ${Math.ceil(end - start)}ms`)

        return { notes };
    }
}

module.exports = {
    pods: [HugoExportPod],
};