import {
    ConfigUtils,
    ConfigService,
    URI,
    DendronConfig,
} from "@dendronhq/common-all";
import { DConfig } from "@dendronhq/common-server"
import {
    MDUtilsV5, ProcFlavor, getParsingDependencyDicts
} from "@dendronhq/unified";
import {
    PublishPod,
    PublishPodPlantOpts, PodUtils
} from "@dendronhq/pods-core";
import { JSONSchemaType } from "ajv";
import { HugoPublishPodConfig, Url } from "./Types"
import { ID } from "./index";
const urls = require('rehype-urls')

export class HugoPublishPod extends PublishPod<HugoPublishPodConfig> {
    static id: string = ID;
    static description: string = "publish (hugo)";

    get config(): JSONSchemaType<HugoPublishPodConfig> {
        return PodUtils.createPublishConfig({
            required: [],
            properties: {},
        }) as JSONSchemaType<HugoPublishPodConfig>;
    }

    async plant(opts: PublishPodPlantOpts<HugoPublishPodConfig>) {
        const { engine, note, config } = opts;
        const {
            names,
            paths,
            ids,
            nav_exclude = "bookHidden",
            nav_collapse = "bookCollapseSection",
            nav_order = "weight",
            asset_prefix = ""
        } = config;

        /* const configReadResult = await ConfigService.instance().readConfig(
            URI.file(engine.wsRoot)
        );
        if (configReadResult.isErr()) {
            throw configReadResult.error;
        }
        const dendronConfig = configReadResult.value; */
        const dendronConfig = DConfig.readConfigSync(engine.wsRoot, false);

        let workspaceConfig = ConfigUtils.getWorkspace(dendronConfig)
        workspaceConfig.enableUserTags = true
        workspaceConfig.enableHashTags = true

        let publishConfig = ConfigUtils.getPublishing(dendronConfig);
        if (asset_prefix) { publishConfig.assetsPrefix = asset_prefix }
        publishConfig.enableBackLinks = false
        publishConfig.enableHierarchyDisplay = false
        publishConfig.enableFMTitle = note.custom?.enableFMTitle as boolean

        const noteCacheForRenderDict = await getParsingDependencyDicts(
            note,
            engine,
            dendronConfig,
            dendronConfig.workspace.vaults
        );

        let rehype = MDUtilsV5.procRehypeFull({
            noteToRender: note,
            noteCacheForRenderDict,
            vault: note.vault,
            vaults: engine.vaults,
            wsRoot: engine.wsRoot,
            fname: note.fname,
            config: dendronConfig,
        }, { flavor: ProcFlavor.PUBLISHING })
            .use(urls, (u: Url) => this.parseLinks(u, ids, asset_prefix)); // replace id links with real links

        // godawful workaround to stop markdown headings turning into <h#> tags 
        // match all hashes until whitespace and replace with ‱(num. hashes)
        let outHeadingPreprocess = note.body.replace(/#+?(?=[\s])/g, x => `‱${x.length}`)
        let outHtml = (await rehype.process(outHeadingPreprocess)).toString()
        // match above change up to </p> or newline and put hashes back and add newlines to satisfy the md parser
        let outHeadingPostprocess = outHtml.replace(
            /‱\d*.+?(?=<\/p>|\n)/g,
            x => `\n\n${x.replace(/‱\d*/, y => "#".repeat(parseInt(y.slice(1))))}\n`
        )

        // children
        let children = Object.keys(names)
            .filter(x => x.startsWith(note.fname) && x.slice(note.fname.length).split('.').length == 2)
            .map(x => paths[x])

        // backlinks
        let backlinks = new Set(note.links
            .filter(x => x.type == "backlink")
            .map(x => paths[x.from.fname as string])
        )

        return [
            `---`,
            `# generated from dendron page`,
            `title: \"${note.title}\"`,
            `desc: \"${note.desc}\"`,
            `updated: ${note.updated}`,
            `created: ${note.created}`,
            `fname: \"${note.fname}\"`,
            `${nav_exclude}: ${note.custom?.nav_exclude || false}`,
            `${nav_collapse}: ${!note.custom?.keep_open && note.children.length > 0}`,
            `${nav_order}: ${note.custom?.nav_order || 0}`,
            `backlinks: [${[...backlinks].map(x => `\"${x}\"`)}]`,
            `children: [${[...children].map(x => `\"${x}\"`)}]`,
            ...Object.keys(note.custom || [])
                .filter(x => !new Set(Object.keys(config)).has(x))
                .map(x => `${x}: \"${note.custom[x]}\"`),
            `---`,
            outHeadingPostprocess || "(this is a stub page)"
        ].join('\n').trim();
    }

    private parseLinks(u: Url, ids: { [key: string]: string }, assetsPrefix: string): string | undefined {
        let link = ids[u.href.split('/').at(-1)!]
        if (link) { link = `${assetsPrefix}${link}` }
        return link
    }
}
